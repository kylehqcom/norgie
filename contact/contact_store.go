package contact

import (
	"gitlab.com/kylehqcom/norgie/config"

	bolt "github.com/johnnadratowski/golang-neo4j-bolt-driver"

	"fmt"

	"time"

	log "github.com/Sirupsen/logrus"
)

const uniqueIndexKey = "upcid"

// The ContactStore denotes methods all User Stores should implement. Note the
type ContactStore interface {
	Contacts(userID string) (UserContacts, error)
	Upsert(ucs *UserContacts) error

	//FindByEmail(string) (*User, error)
	//FindBySub(int64) (*User, error)
	//Save(User) (*User, error)
}

var (
	globalContactStore neo4jDBContactStore
	connectionTimeout  time.Duration
)

// A neo4jDBContactStore is used to interact between neo4j and a Norgie contacts
type neo4jDBContactStore struct {
	Pool bolt.DriverPool
}

func BootstrapDB() {
	log.Debug("Bootstrapping Neo4j contact store")

	neo4jConfig := config.Instance().Sub("database.neo4j")
	dsnConfig := neo4jConfig.Sub("dsn")

	u := dsnConfig.GetString("user")
	p := dsnConfig.GetString("password")
	host := dsnConfig.GetString("host")
	port := dsnConfig.GetString("port")

	// Check for a user/password combination
	up := ""
	if u != "" {
		up = fmt.Sprintf("%s:%s@", u, p)
	}

	dsn := fmt.Sprintf("bolt://%s%s:%s", up, host, port)
	log.Debug(fmt.Sprintf("Opening Neo4j on bolt://%s:%s", host, port))

	// Set the connection timeout
	t := neo4jConfig.GetString("timeout")
	if t == "" {
		t = "10s"
	}

	d, err := time.ParseDuration(t)
	if err != nil {
		d, _ = time.ParseDuration("10s")
	}
	connectionTimeout = d

	poolMax := neo4jConfig.GetInt("driver_pool_limit")
	if poolMax == 0 {
		poolMax = 10
	}

	driverPool, err := bolt.NewDriverPool(dsn, poolMax)
	if err != nil {
		// Fatal this - we need a dbase
		log.WithFields(log.Fields{
			"dsn": neo4jConfig.GetString("dsn"),
		}).WithError(err).Fatal("Unable to create neo4j session")
	}

	globalContactStore = neo4jDBContactStore{Pool: driverPool}
	log.Debug("Bootstrap Neo4j contact store complete")

	// Ensure we have the correct indexes on our Graphs
	globalContactStore.ensureIndexes()
}

// GlobalContactStore returns Norgies global store for contact persistence.
func GlobalContactStore() neo4jDBContactStore {
	// Bootstrap the neo4jDB if none found
	if globalContactStore.Pool == nil {
		BootstrapDB()
	}

	return globalContactStore
}

// conn will return a connection from the pool
func (gcs neo4jDBContactStore) conn() (bolt.Conn, error) {
	conn, err := gcs.Pool.OpenPool()
	if err != nil {
		log.WithError(err).Error("Unable to get neo4j connection from pool")
		return conn, err
	}

	// Assign the default connection timeout.
	conn.SetTimeout(connectionTimeout)
	return conn, nil
}

// ensureIndexes will create the required neo4j indexes. There is no easy Cypher query to
// check whether an index already exists. Interactively you can ":schema ls" which will return index info.
// Alternatively you can call the api via an http request to return index data. But you can re-create an
// index with no harm and no changes. Therefore we create all every time.
//
// Also note that neo4j does not (yet) support compound unique keys but you can concatenate values
// to get the desired result.
func (n neo4jDBContactStore) ensureIndexes() {
	log.Debug("Call: neo4jDBContactStore.ensureIndexes()")
	c, err := n.conn()
	if err != nil {
		log.WithError(err).Error("Unable to create neo4j indexes")
		return
	}
	defer c.Close()

	var totalAdded int64
	for _, v := range []string{
		fmt.Sprintf("CREATE CONSTRAINT ON (c:Contact) ASSERT c.%s IS UNIQUE", uniqueIndexKey),
		"CREATE INDEX ON :Contact(user_id)",
		"CREATE INDEX ON :Contact(provider)",
	} {
		res, err := c.ExecNeo(v, nil)
		if err != nil {
			log.WithError(err).WithFields(log.Fields{
				"query": v,
			}).Error("Unable to create neo4j index")
		}

		meta := res.Metadata()
		if stats, ok := meta["stats"].(map[string]interface{}); ok {
			// Check unique constraint indexes.
			if added, ok := stats["constraints-added"]; ok {
				totalAdded += added.(int64)
			}

			// Check for regular indexes.
			if added, ok := stats["indexes-added"]; ok {
				totalAdded += added.(int64)
			}
		}
	}

	idxString := func() string {
		switch totalAdded {
		case 0:
			return "No new indexes added."
		case 1:
			return "Added one new index."
		default:
			return fmt.Sprintf("Added %d new indexes.", totalAdded)
		}
	}()

	log.Debug(fmt.Sprintf("neo4jDBContactStore.ensureIndexes complete. %s", idxString))
}

// Upsert will create a new entry or merge an existing. Although a merge is
// not as performant as a direct new insert, it is safer. And due to the average
// contact count a user is to have, it should be fast enough. Especially with the
// indexes in place.
//
// Also to note, the query query creation must match the query params. If looking to
// place into a go routine, these must be kept in sync/order.
func (n neo4jDBContactStore) Upsert(ucs *UserContacts) ([]bolt.Result, error) {

	log.Debug("Call: neo4jDBContactStore.Upsert()")

	start := time.Now()
	c, err := n.conn()
	defer c.Close()

	queryTemplate := fmt.Sprintf("MERGE (c:Contact { %s: {idx_value}}) SET ", uniqueIndexKey) +
		"c.email = {email}, " +
		"c.full_name = {full_name}, " +
		"c.user_id = {user_id}, " +
		"c.provider = {provider} "

	var queries []string
	var queryParams []map[string]interface{}

	for provider, ps := range ucs.Providers {
		cLen := len(ps.Contacts)
		log.Debug(fmt.Sprintf("Found %d contact(s) to process.", cLen))
		for _, contact := range ps.Contacts {

			queryIteration := queryTemplate

			queryParam := make(map[string]interface{})
			queryParam["idx_value"] = fmt.Sprintf("%s%s%s", ucs.UserID, provider, contact.ID)
			queryParam["email"] = contact.Email
			queryParam["full_name"] = contact.FullName
			queryParam["user_id"] = ucs.UserID
			queryParam["provider"] = provider
			queryParam["id"] = contact.ID

			// Delete any existing manager relations to reassign
			queryIteration += "WITH c " +
				"OPTIONAL MATCH (m)-[existing:MANAGER]->(c) " +
				"DELETE m, existing "

			// Assign the new manager relations using a unique param key.
			for k, m := range contact.Managers {
				pk := fmt.Sprintf("m%d%s", k, contact.ID)
				queryIteration += fmt.Sprintf("MERGE (c)<-[:MANAGER]-(:Manager { manager: {%s} })", pk)
				queryParam[pk] = m.Name
			}

			queryParams = append(queryParams, queryParam)
			queries = append(queries, queryIteration)
		}
	}

	queryCount := len(queries)
	if 0 == queryCount {
		log.Debug("No upsert required as no User contacts present to process")
		return nil, nil
	}

	pipeline, err := c.PreparePipeline(queries...)
	if err != nil {
		log.WithError(err).Error("Prepare pipeline error.")
		return nil, err
	}

	res, err := pipeline.ExecPipeline(queryParams...)
	if err != nil {
		log.WithError(err).Error("Upsert contact failure.")
	}

	// Clean up
	pipeline.Close()

	log.Debug(fmt.Sprintf("Upsert of %d processed in %f seconds.", queryCount, time.Since(start).Seconds()))
	return res, err
}

//// Find will return a user from a mongoDB generated UUID
//func (m mongoDBUserStore) Find(ID bson.ObjectId) (User, error) {
//	u := User{}
//	err := m.db().C(UserCollection).FindId(ID.Hex()).One(&u)
//	return u, err
//}
//
//// FindByEmail will attempt to return a User by a "google" email address.
//func (m mongoDBUserStore) FindByEmail(email string) (User, error) {
//	u := User{}
//	err := m.db().C(UserCollection).Find(bson.M{"email": email}).One(&u)
//	return u, err
//}
//
//// FindBySub will attempt to return a User by a 3rd party ID, their "sub"
//func (m mongoDBUserStore) FindBySub(sub int64) (User, error) {
//	u := User{}
//	err := m.db().C(UserCollection).Find(bson.M{"sub": sub}).One(&u)
//	return u, err
//}
//
//// Save will persist a User to the MongoDB via an Upsert
//func (m mongoDBUserStore) Save(u User) (User, error) {
//	if !u.ID.Valid() {
//		u.ID = bson.NewObjectId()
//	}
//
//	_, err := m.db().C(UserCollection).UpsertId(u.ID, bson.M{"$set": u})
//	if mgo.IsDup(err) {
//		log.WithFields(log.Fields{
//			"user": u,
//		}).WithError(err).Error("Duplicate user key found")
//
//		// And reset error as we have handled
//		err = nil
//	}
//
//	return u, err
//}
