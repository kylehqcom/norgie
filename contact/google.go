package contact

import "time"

// NewGoogleContacts will return a new Google Contact struct instance.
func NewGoogleContacts() *GoogleContacts {
	return &GoogleContacts{}
}

// GoogleContacts is the housing struct for an array of Google contact objects.
type GoogleContacts struct {
	gFeed `json:"feed"`
}

type gFeed struct {
	ETag    string `json:"gd$etag"`
	Updated struct {
		T time.Time `json:"$t"`
	} `json:"updated"`
	TotalResults struct {
		T int64 `json:"$t,string"`
	} `json:"openSearch$totalResults"`
	ResultsStartIndex struct {
		T int64 `json:"$t,string"`
	} `json:"openSearch$startIndex"`
	ResultsPerPage struct {
		T int64 `json:"$t,string"`
	} `json:"openSearch$itemsPerPage"`
	Entry []gContact `json:"entry"`
}

type gContact struct {
	ETag    string `json:"gd$etag"`
	Updated struct {
		T time.Time `json:"$t"`
	} `json:"updated"`
	ID struct {
		T string `json:"$t"`
	} `json:"id"`
	Names         gNameEntries    `json:"gd$name"`
	Emails        []gEmail        `json:"gd$email"`
	PhoneNumbers  []gPhoneNumber  `json:"gd$phoneNumber"`
	Relations     []gRelations    `json:"gContact$relation"`
	CustomFilters []gCustomFilter `json:"gContact$userDefinedField"`
}

type gNameEntries struct {
	FirstName struct {
		T string `json:"$t"`
	} `json:"gd$givenName"`
	LastName struct {
		T string `json:"$t"`
	} `json:"gd$familyName"`
	FullName struct {
		T string `json:"$t"`
	} `json:"gd$fullName"`
}

type gEmail struct {
	Address string `json:"address"`
	Primary bool   `json:"primary,string"`
}

type gPhoneNumber struct {
	PhoneNumber string `json:"uri"`
}

type gRelations struct {
	Name     string `json:"$t"`
	Relation string `json:"rel"`
}

type gCustomFilter struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}
