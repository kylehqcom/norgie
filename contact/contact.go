package contact

import (
	"time"
)

// Contact is the default generic struct that represents a Contact inside Norgie
type Contact struct {
	ID       string    `json:"id"`
	Email    string    `json:"email"`
	FullName string    `json:"fullname"`
	Managers []Manager `json:"managers,omitempty"`
}

// Manager is a struct representing a Manager to a contact
type Manager struct {
	Name string `json:"name"`
}

// ProviderContacts are a struct that link UserContacts with Contact objects.
type ProviderContacts struct {
	Updated  time.Time
	Contacts []*Contact `json:"contacts"`
}

// UserContacts is a struct containing a Provider index on Contact objects.
type UserContacts struct {
	UserID    string
	Providers map[string]*ProviderContacts
}

// NewContact will return a new Contact instance.
func NewContact() *Contact {
	return &Contact{}
}

// NewProviderContacts will return a new Provider Contact instance.
func NewProviderContacts() *ProviderContacts {
	return &ProviderContacts{}
}

// NewUserContacts will return a new User Contact instance.
func NewUserContacts() *UserContacts {
	ps := make(map[string]*ProviderContacts)
	return &UserContacts{
		Providers: ps,
	}
}

// IsManaged will return whether this Contact instance has a manager.
func (c Contact) IsManaged() bool {
	return 0 < len(c.Managers)
}

// ByProviderKey will return an array of Contacts associated to a user by a Provider key.
// No error is returned on invalid key, just an empty array
func (uc UserContacts) ByProviderKey(key string) []*Contact {
	c := []*Contact{}
	res, ok := uc.Providers[key]
	if !ok {
		return c
	}

	return res.Contacts
}
