package transport

import (
	"net/http"

	"gitlab.com/kylehqcom/norgie/config"
	"gitlab.com/kylehqcom/norgie/store"

	"encoding/gob"
	"io/ioutil"

	"encoding/json"

	"fmt"

	"strconv"

	log "github.com/Sirupsen/logrus"
	"github.com/gorilla/sessions"
	goauth2 "golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

type googleOauth struct {
	Config *goauth2.Config
}

const googleSessionName = "norgie-google-session"

var googleAuthStore = sessions.NewCookieStore([]byte(config.Instance().GetString("session.cookie_secret")))

func init() {
	googleAuthStore.Options = &sessions.Options{
		Path:     "/auth/oauth2/callback/google",
		MaxAge:   300, // 5 mins should be plenty to accept, no need to keep this around
		HttpOnly: true,
	}

	gob.Register(&goauth2.Token{})
}

func NewGoogleClient(t *goauth2.Token) *http.Client {
	return NewGoogleOauth().Config.Client(goauth2.NoContext, t)
}

func NewGoogleOauth() *googleOauth {
	googleOauthConfig := config.Instance().Sub(config.OAUTH_GOOGLE)
	relativeUrl := googleOauthConfig.GetString(config.REDIRECT_URL)
	c := &goauth2.Config{
		ClientID:     googleOauthConfig.GetString(config.CLIENT_ID),
		ClientSecret: googleOauthConfig.GetString(config.CLIENT_SECRET),
		RedirectURL:  config.Instance().GetString(config.SITE_HTTP_ROOT) + relativeUrl,
		Scopes: []string{
			"https://www.googleapis.com/auth/contacts.readonly",
			"email",
			"openid",
			"profile",
		},
		Endpoint: google.Endpoint,
	}

	return &googleOauth{
		Config: c,
	}
}

func GoogleOauth2(w http.ResponseWriter, r *http.Request) {
	log.Debug("Call: transport.GoogleAuth2")
	s, err := googleAuthStore.Get(r, googleSessionName)
	if err != nil {
		// Create new if err
		s, _ = googleAuthStore.New(r, googleSessionName)
		log.WithError(err).Warning("New Google session created")
	}

	if s.IsNew {
		s.Values["session_id"] = store.GenerateID()
		log.Debug("New session_id generated")
	}

	// Check for a rel
	rel := r.URL.Query().Get("rel")
	if rel == "" {
		rel = "/view"
		log.Debug("Empty rel, defaulting to /view")
	}
	s.Values["rel"] = rel

	// Check for a force offline approval param
	f := r.URL.Query().Get("force")
	force := false
	if f != "" {
		force, _ = strconv.ParseBool(f)
	}
	log.Debug(fmt.Sprintf("Offline approval force value set to %s", force))

	// Always generate a new Xsrf
	sid := s.Values["session_id"].(string)
	xsrf := store.GenerateXsrf(sid, r.URL.Path)
	s.Values["xsrf"] = xsrf
	err = s.Save(r, w)
	if err != nil {
		log.WithError(err).Error("Unable to save Google session")
		s.AddFlash("We were unable to authenticate with Google, please try again.")
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	// Redirect the user
	goog := NewGoogleOauth()
	url := goog.Config.AuthCodeURL(xsrf, goauth2.AccessTypeOffline)
	if force {
		url = goog.Config.AuthCodeURL(xsrf, goauth2.AccessTypeOffline, goauth2.ApprovalForce)
	}

	http.Redirect(w, r, url, http.StatusFound)
}

func GoogleOauth2Callback(w http.ResponseWriter, r *http.Request) {
	log.Debug("GoogleCallback")
	s, err := googleAuthStore.Get(r, googleSessionName)
	if err != nil {
		log.WithError(err).Error("Unable to read Google session")
		// Redirect, we are on the callback so need a valid session
		redirectOauthError(w, r)
		return
	}

	params := r.URL.Query()
	state := params.Get("state")
	xsrf, ok := s.Values["xsrf"].(string)
	if !ok || state != xsrf {
		log.WithFields(log.Fields{
			"state": state,
			"xsrf":  xsrf,
		}).Error("xsrf and state mismatch on Google oauth")

		// Let the user know we had an issue too
		s.AddFlash("We were unable to confirm your details with Google.")
		s.Save(r, w)

		redirectOauthError(w, r)
		return
	}
	log.Debug("Google xsrf all ok")

	code := params.Get("code")
	if code == "" {
		log.WithFields(log.Fields{
			"state": state,
			"xsrf":  xsrf,
		}).Error("No code sent from Google oauth")

		// Let the user know we had an issue too
		s.AddFlash("We were unable to confirm your details with Google.")
		s.Save(r, w)

		redirectOauthError(w, r)
		return
	}
	log.Debug("Google code all ok")

	goog := NewGoogleOauth()
	tok, err := goog.Config.Exchange(goauth2.NoContext, code)
	if err != nil {
		log.WithFields(log.Fields{
			"code": code,
			"xsrf": xsrf,
		}).WithError(err).Error("Bad oauth google exchange")

		// Let the user know we had an issue too
		s.AddFlash("We were unable to confirm your details with Google.")
		s.Save(r, w)

		redirectOauthError(w, r)
		return
	}
	log.Debug("Google exchange all ok")

	// Get the global session
	globalSessionStore, err := store.Session(r)
	if err != nil {
		log.WithFields(log.Fields{
			"state": state,
			"xsrf":  xsrf,
		}).WithError(err).Error("Unable to get the user session")

		// Let the user know we had an issue too - ignoring any err
		s.AddFlash("We were unable to confirm your details with Google.")
		s.Save(r, w)

		redirectOauthError(w, r)
		return
	}

	// Save token to session
	globalSessionStore.Values["token"] = tok
	globalSessionStore.Save(r, w)

	rel, ok := s.Values["rel"].(string)
	if !ok || rel == "" {
		rel = "/"
	}

	// Also delete/expire the googleAuthStore info now we have auth'd ok
	s.Options.MaxAge = -1
	err = s.Save(r, w)
	if err != nil {
		log.WithError(err).Error("Google session save error when trying to expire")
	}
	log.Debug(fmt.Sprintf("rel should be %s", rel))

	http.Redirect(w, r, rel, http.StatusFound)
}

type GoogleUserInfo struct {
	Sub     string        `json:"sub"`
	Name    string        `json:"name"`
	Email   string        `json:"email"`
	Picture string        `json:"picture"`
	Domain  string        `json:"hd"`
	Token   goauth2.Token `json:"token"`
}

func GetUserInfo(t *goauth2.Token) (GoogleUserInfo, error) {
	res, err := NewGoogleClient(t).Get("https://www.googleapis.com/oauth2/v3/userinfo?alt=json")
	gu := GoogleUserInfo{Token: *t}

	// Close the response
	defer func() {
		if res.Body != nil {
			res.Body.Close()
		}
	}()

	if err != nil {
		log.WithError(err).Error("Bad google user info result")
		return gu, err
	}

	bytes, err := ioutil.ReadAll(res.Body)
	err = json.Unmarshal(bytes, &gu)
	if err != nil {
		log.WithError(err).Error("Error on Google user json unmarshal")
	}

	log.Debug("Google user details all ok")
	return gu, err
}

func redirectOauthError(w http.ResponseWriter, r *http.Request) {
	s, _ := googleAuthStore.Get(r, store.SessionName())
	s.AddFlash("We were unable to authenticate with Google, please try again.")
	http.Redirect(w, r, "/", http.StatusFound)
}
