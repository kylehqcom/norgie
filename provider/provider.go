package provider

import (
	"time"

	"golang.org/x/oauth2"

	"gitlab.com/kylehqcom/norgie/config"
	"gitlab.com/kylehqcom/norgie/contact"
)

const (
	ProviderGoogle = "google"
)

var DefaultProviders = make(map[string]*Provider)

func init() {
	// Assign default providers
	g := NewGoogle()
	DefaultProviders[g.ProviderKey()] = g
}

type FetchContacts interface {
	ContactsExpired() bool
	Fetch(chan<- *FetchResult, *FetchParams)
	ProviderKey() string
}

type FetchParams struct {
	CurrentIndex int64
	LastUpdated  time.Time
	Token        oauth2.Token
	Reset        bool
}

type FetchResult struct {
	Result *contact.ProviderContacts
	Error  error
}

// NewFetchParams will return a new pointer instance to a yield param struct.
func NewFetchParams() *FetchParams {
	return &FetchParams{CurrentIndex: 1}
}

// NewFetchParams will return a new pointer instance to a yield param struct.
func NewFetchResult() *FetchResult {
	return &FetchResult{}
}

// Provider is a Contact provider instance
type Provider struct {
	Name    string    `bson:"name"`
	Updated time.Time `bson:"updated"` // The last updated feed
	Filters map[string]Filter
}

type Filter struct {
	Key   string `bson:"key"`
	Value string `bson:"value"`
}

// HasSyncd will determine whether a provider has sync'd with it's source based on an eTag or Updated.
func (p Provider) HasSyncd() bool {
	return !p.Updated.IsZero()
}

// ContactsExpired will return true if the updated sync time breaches our default expiration or never sync'd
func (p Provider) ContactsExpired() bool {
	if p.HasSyncd() {
		duration, _ := time.ParseDuration(config.Instance().GetString("contacts.sync.expiration"))
		expires := p.Updated.Add(duration)
		return time.Now().After(expires)
	}
	return true
}
