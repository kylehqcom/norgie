package provider

import (
	log "github.com/Sirupsen/logrus"
	"gitlab.com/kylehqcom/norgie/contact"
	"gitlab.com/kylehqcom/norgie/errors"
	"gitlab.com/kylehqcom/norgie/transport"

	"net/http"

	"encoding/json"
	"fmt"
	"io/ioutil"
	"strings"
	"time"
)

const (
	MaxResultsPerPage = 1000
)

// NewGoogle will create and return a new Google Provider instance
func NewGoogle() *Provider {
	return &Provider{
		Name: ProviderGoogle,
	}
}

func (p *Provider) ProviderKey() string {
	return ProviderGoogle
}

func (p *Provider) Fetch(c chan<- *FetchResult, fp *FetchParams) {
	log.Debug("Call: google.Fetch()")

	// Add the Provider values to the Fetch Params
	fp.LastUpdated = p.Updated

	// Create a new fetch result to return on the channel.
	res := NewFetchResult()

	// First get the raw data from Google, results will vary based on number updated
	gcs, err := get(fp)
	if err != nil {
		res.Error = err
		c <- res
		return
	}

	// Transform the gContacts into the global contact struct
	contacts, err := transform(gcs)
	if err != nil {
		res.Error = err
		c <- res
		return
	}

	// Update the last updated on the provider contacts. If no results returned, assign now.
	providerContacts := contact.NewProviderContacts()
	if gcs.Updated.T.IsZero() {
		providerContacts.Updated = time.Now()
	} else {
		providerContacts.Updated = gcs.Updated.T
	}
	providerContacts.Contacts = contacts

	// Assign on the fetch result and send down the channel
	res.Result = providerContacts
	c <- res
}

// generateRequest will create a new http.Request object to retrieve Google contacts
// Refer https://developers.google.com/google-apps/contacts/v3/reference#contacts-query-parameters-reference
func generateRequest(fp *FetchParams) (*http.Request, error) {
	// Set the base uri, getting the most recently updated contacts last
	uri := "https://www.google.com/m8/feeds/contacts/default/full" +
		"?alt=json" +
		"&showdeleted=true" +
		"&orderby=lastmodified" +
		"&sortorder=ascending"

	// Add the current index
	uri = fmt.Sprintf("%s&start-index=%d", uri, fp.CurrentIndex)

	// Set the max per page, setting this internally helps pagination on processing
	uri = fmt.Sprintf("%s&max-results=%d", uri, MaxResultsPerPage)

	// Apply any minimum updated_at to filter results
	if !fp.LastUpdated.IsZero() {
		// Done because my mac will turn the correct RFC 2009-11-10T23:00:00Z into
		// 2016-06-30T19:54:31+01:00 Grrr!!! Also subtracting a day to be sure =]
		lu := fp.LastUpdated.AddDate(0, 0, -1).Format(time.RFC3339)
		i := strings.Index(lu, "+")
		if i > 0 {
			lu = fmt.Sprintf("%sZ", lu[:i])
		}
		uri = fmt.Sprintf("%s&updated-min=%s", uri, lu)
	}

	log.Debug(fmt.Sprintf("Calling google api endpoint with %s", uri))

	// Google prefers setting the version via a header over a query param. Refer
	// https: //developers.google.com/google-apps/contacts/v3/#specifying_a_version
	req, err := http.NewRequest(http.MethodGet, uri, nil)
	if err != nil {
		log.WithError(err).Error("Unable to create Google contact api request object")
		return nil, err
	}
	req.Header.Set("GData-Version", "3.0")
	return req, nil
}

// get will return a populated GoogleContacts struct, taking into consideration the last updated time
// to only returned current results. Note that there are NO waitGroups or Go Routines here. Just a regular
// old for loop. This is because "most" users are going to have less contacts than the max per page value
// of 1000. In order to run concurrent requests, you need to know the total contacts. You gain this info from
// the first request. So why not just only call one request instead. The best/fastest code is no code!
func get(fp *FetchParams) (*contact.GoogleContacts, error) {
	all := contact.NewGoogleContacts()
	client := transport.NewGoogleClient(&fp.Token)

	var (
		totalResults int64
		safety       int64
	)

	start := time.Now()
	for fp.CurrentIndex < totalResults || totalResults == 0 {

		// For loops without breaks can be evil...
		safety = safety + 1

		gcs := contact.NewGoogleContacts()
		req, err := generateRequest(fp)
		if err != nil {
			log.WithError(err).Error("Unable to create Google contact request object")
			return all, err
		}

		// Call the request url for a response.
		res, err := client.Do(req)
		if err != nil {
			log.WithFields(log.Fields{"token": fp.Token}).WithError(err).Error("Error calling Google contacts api")
			return all, err
		}

		// Close the response
		defer func() {
			if res.Body != nil {
				res.Body.Close()
			}
		}()

		bytes, err := ioutil.ReadAll(res.Body)
		if err != nil {
			log.WithError(err).WithFields(log.Fields{"params": fp}).Error("Unable to read Google contact response")
			return all, err
		}

		err = json.Unmarshal(bytes, &gcs)
		if err != nil {
			log.WithError(err).Error("Unable to parse json gContacts")
			return all, err
		}

		// Assign this iterations entries to the all
		all.Entry = append(all.Entry, gcs.Entry...)

		// Now update the fetchParam index for the next iteration in a mutex lock to ensure
		nextIndex := gcs.ResultsStartIndex.T + MaxResultsPerPage
		fp.CurrentIndex = nextIndex

		// Confirm the total results
		totalResults = gcs.TotalResults.T
		log.Debug(fmt.Sprintf("Total contact results: %d", totalResults))

		// Also assign the updated value from this payload
		if all.Updated.T.IsZero() {
			all.Updated.T = gcs.Updated.T
		}

		// Break if we have no results
		if totalResults == 0 {
			break
		}

		// Just in case we go infinite =]
		if safety > 10 {
			// This would mean a user with 10K contacts!!! Get their monies =]
			// Code this so that the return value enforces upgrades etc
			return all, errors.ErrTooManyRequests
		}
	}
	log.Debug(fmt.Sprintf("With %d per page, it took %f seconds to retrieve contacts.", MaxResultsPerPage, time.Since(start).Seconds()))
	return all, nil
}

func transform(gcs *contact.GoogleContacts) ([]*contact.Contact, error) {
	contacts := []*contact.Contact{}
	for _, v := range gcs.Entry {
		c := contact.NewContact()
		emails := v.Emails
		for _, v := range emails {
			if v.Primary || 1 == len(emails) {
				c.Email = v.Address
				break
			}
		}

		// Check for the explicit Relations struct.
		for _, rel := range v.Relations {
			if rel.Relation == "manager" {
				c.Managers = append(c.Managers, contact.Manager{rel.Name})
			}
		}

		// Some contacts do not have a full nme value so default with the email address.
		if v.Names.FullName.T == "" {
			c.FullName = c.Email
		} else {
			c.FullName = v.Names.FullName.T
		}

		ID := strings.Split(v.ID.T, "/")
		c.ID = ID[len(ID)-1]
		contacts = append(contacts, c)
	}

	return contacts, nil
}
