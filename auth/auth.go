package auth

import (
	"net/http"
	"strconv"

	log "github.com/Sirupsen/logrus"

	"fmt"

	"github.com/gorilla/sessions"
	"gitlab.com/kylehqcom/norgie/errors"
	"gitlab.com/kylehqcom/norgie/store"
	"gitlab.com/kylehqcom/norgie/transport"
	usr "gitlab.com/kylehqcom/norgie/user"
	"golang.org/x/oauth2"
)

func assignUserDetails(userInfo transport.GoogleUserInfo) *usr.User {
	log.Debug("Assigning user details from Google user info")
	u := usr.NewUser()

	// Check if an existing user already exists
	userStore := usr.GlobalUserStore()
	sub, _ := strconv.ParseInt(userInfo.Sub, 10, 64)
	existU, _ := userStore.FindBySub(sub)
	if existU.ID.Valid() {
		// Assign the existing id to the latest user details returned
		log.Debug(fmt.Sprintf("Assigning an existing user found from database with id: %s", existU.ID))
		u.ID = existU.ID
	}

	// Assign token values.
	u.Token.AccessToken = userInfo.Token.AccessToken
	u.Token.TokenType = userInfo.Token.TokenType
	u.Token.Expiry = userInfo.Token.Expiry

	// Assign a new refresh token, but don't assign an empty token.
	if "" != userInfo.Token.RefreshToken {
		log.Debug("Assigning a new refresh token")
		u.Token.RefreshToken = userInfo.Token.RefreshToken
	} else if "" != existU.Token.RefreshToken {
		log.Debug("Assigning an existing refresh token")
		u.Token.RefreshToken = existU.Token.RefreshToken
	}

	if "" == u.Token.RefreshToken {
		log.WithFields(
			log.Fields{
				"user_info": userInfo,
				"user":      u,
			}).Error("No refresh token assigned for user")
	}

	u.Sub = sub
	u.Email = userInfo.Email
	u.Domain = userInfo.Domain
	u.Name = userInfo.Name
	u.Avatar = userInfo.Picture

	// Now store this user to the database
	savedUser, err := userStore.Save(*u)
	if err != nil {
		log.WithFields(log.Fields{"user": u}).WithError(err).Error("Unable to save user to global user store")
	}

	return savedUser
}

func Authenticate(w http.ResponseWriter, r *http.Request) {
	s, err := GetSession(r)
	if err != nil {
		log.WithError(err).Error("Error parsing session")
		s.AddFlash("We have had an internal error attempting to authorise your access. Please try again.")
		s.Save(r, w)
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	// Error types alter user interaction.
	err = confirmAuthSession(s, w, r)
	if err != nil {
		authUrl := GenerateOauthUrl(r)
		if errors.IsUserError(err) {
			if err == errors.ErrUserMissingRefreshToken {
				log.WithError(err).Warning("No refresh token for user.")

				// Assign the force param to ensure a refresh token is assigned. Self cleaning.
				authUrl = fmt.Sprintf("%s%s", authUrl, "&force=true")
				http.Redirect(w, r, authUrl, http.StatusFound)
				return
			}

			if err == errors.ErrUserTokenInvalid {
				log.WithError(err).Info("Invalid token for user, normally due to token expiration.")
				http.Redirect(w, r, authUrl, http.StatusFound)
				return
			}

			if err == errors.ErrNoUserId {
				log.WithError(err).Info("No user id assigned so authenticate user.")
				http.Redirect(w, r, authUrl, http.StatusFound)
				return
			}

			if err == errors.ErrUserNotFound {
				log.WithError(err).Error("No user found in database from id.")

				// This time send back to homepage, if we have a user id in session
				// but none in the dbase, then saving to the dbase must have failed so
				// signout this user to clear any values & stop sending in a redirect loop
				s.AddFlash("We were unable to confirm your details with Google.")
				s.Save(r, w)
				http.Redirect(w, r, "/signout", http.StatusFound)
				return
			}
		}

		// An unknown error occurred.
		log.WithError(err).WithFields(log.Fields{"session": s}).Error("Unknown error on secure call.")
		s.AddFlash("We have had an internal error attempting to authorise your access. Please try again.")
		s.Save(r, w)
		http.Redirect(w, r, "/signout", http.StatusFound)
		return
	}
}

func confirmAuthSession(s *sessions.Session, w http.ResponseWriter, r *http.Request) error {

	u := usr.NewUser()

	// Does the session have a valid token, check first as this only occurs on a successful oauth2 flow.
	tok := TokenFromSession(s)
	if tok.Valid() {

		// Since we have a token in session, retrieve more user details
		userInfo, err := transport.GetUserInfo(tok)
		if err != nil {
			log.WithFields(log.Fields{"token": tok}).WithError(err).Error("Unable to GetUserInfo")
		} else {
			u = assignUserDetails(userInfo)
			s.Values["user_id"] = u.ID.Hex()
		}

		// Unset the token from session and set authenticated true
		s.Values["token"] = nil
		err = s.Save(r, w)
		if err != nil {
			log.WithFields(log.Fields{"session": s}).WithError(err).Error("Unable to save global session")
			return err
		}
	}

	// Check for a user from token
	if !u.ID.Valid() {
		u, err := UserFromSession(s)
		if err != nil {

			// We have a user error so ensure session user_id values too
			if UserIdFromSession(s) != "" {
				// If we have not found a user, ensure the user_id is removed from session also.
				s.Values["user_id"] = ""
				_ = s.Save(r, w)
			}

			// Do we have a user but the token is no longer valid
			if !u.Token.Valid() {
				// https://developers.google.com/identity/protocols/OAuth2WebServer#refresh
				return errors.ErrUserTokenInvalid
			}

			// Check for a refresh token.
			if u.Token.RefreshToken == "" {
				return errors.ErrUserMissingRefreshToken
			}

			// Return user error
			return err
		}
	}

	return nil
}

// IsAuthenticated confirms that the current user in context is authenticated.
func IsAuthenticated(r *http.Request) bool {
	s, err := GetSession(r)
	if err != nil {
		return false
	}

	u, err := UserFromSession(s)
	if err != nil {
		return false
	}
	return u.ID != "" && u.Token.Valid()
}

// GenerateOauthUrl will return a string url representation for oauth2 redirects
func GenerateOauthUrl(r *http.Request) string {
	return fmt.Sprintf("/auth/oauth2/google?rel=%s", r.URL)
}

func GetSession(r *http.Request) (*sessions.Session, error) {
	s, err := store.Session(r)
	if err != nil {
		log.WithError(err).Error("Error on session read")
		return nil, err
	}

	return s, nil
}

// TokenFromSession will attempt to retrieve a valid oauth token from a active session.
func TokenFromSession(s *sessions.Session) *oauth2.Token {
	t, ok := s.Values["token"].(*oauth2.Token)
	if !ok {
		return &oauth2.Token{}
	}

	return t
}

// UserIdFromSession will attempt to retrieve a valid user_id from a active session.
func UserIdFromSession(s *sessions.Session) string {
	userID, ok := s.Values["user_id"].(string)
	if !ok {
		return ""
	}
	return userID
}

// UserFromRequest will attempt to retrieve a valid User from a http.Request.
func UserFromRequest(r *http.Request) (*usr.User, error) {
	u := usr.NewUser()
	s, err := GetSession(r)
	if err != nil {
		return u, err
	}

	return UserFromSession(s)
}

// UserFromSession attempts to return the authenticated user confirmed via the database.
func UserFromSession(s *sessions.Session) (*usr.User, error) {
	u := usr.NewUser()
	userID := UserIdFromSession(s)
	if "" == userID {
		log.Debug("No user.ID assigned in user session")
		return u, errors.ErrNoUserId
	}

	dbUser, err := usr.GlobalUserStore().Find(userID)
	if err == usr.NotFound {
		log.WithFields(log.Fields{
			"user_id": userID,
		}).Error("No user found in database")
		return u, err
	} else if err != nil {
		log.WithFields(
			log.Fields{
				"user_id": userID,
			}).
			WithError(err).Error("Error retrieving user from database")
		return u, err
	}

	return dbUser, nil
}

// SignOut handler will remove the user session.
func SignOut(w http.ResponseWriter, r *http.Request) {
	// Get the current session
	s, err := store.Session(r)
	if err != nil {
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}

	// To delete, set the current session to the past.
	s.Options.MaxAge = -1
	err = s.Save(r, w)
	if err != nil {
		log.WithError(err).Error("Unable to delete session")
	}

	http.Redirect(w, r, "/", http.StatusFound)
}
