package templates

import (
	"bytes"
	"fmt"
	"html/template"
	"net/http"
	"strconv"

	"encoding/json"

	log "github.com/Sirupsen/logrus"

	"gitlab.com/kylehqcom/norgie/auth"
	"gitlab.com/kylehqcom/norgie/contact"
	"gitlab.com/kylehqcom/norgie/provider"
	"gitlab.com/kylehqcom/norgie/transport"
)

var layoutFuncs = template.FuncMap{
	"yield": func() (string, error) {
		return "", fmt.Errorf("yield called inappropriately")
	},
}

var layout = template.Must(
	template.New("layout.html").Funcs(layoutFuncs).ParseFiles("templates/layout.html"),
)

var templates = template.Must(template.New("t").ParseGlob("templates/**/*.html"))

var errorTemplate = `
<html>
	<body>
		<h1>Error rendering template %s</h1>
		<p>%s</p>
	</body>
</html>
`

func RenderTemplate(w http.ResponseWriter, r *http.Request, name string, data map[string]interface{}) {
	if data == nil {
		data = map[string]interface{}{}
	}

	session, _ := auth.GetSession(r)
	data["Flashes"] = session.Flashes()

	user, err := auth.UserFromSession(session)
	if err == nil {
		data["CurrentUser"] = user
	}

	funcs := template.FuncMap{
		"yield": func() (template.HTML, error) {
			buf := bytes.NewBuffer(nil)
			err := templates.ExecuteTemplate(buf, name, data)
			return template.HTML(buf.String()), err
		},
	}

	layoutClone, _ := layout.Clone()
	layoutClone.Funcs(funcs)
	err = layoutClone.Execute(w, data)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, errorTemplate, name, err.Error())
	}
}

func Secure(h http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		log.Debug(fmt.Sprintf("Secure method called from: %s", r.URL))
		if !auth.IsAuthenticated(r) {
			log.Debug("User is not authenticated on secure call.")

			// Refresh token 1/T1frukvNj7OFjTQgkfI-wYmSO7cg3iak3jg2xbJV05k

			auth.Authenticate(w, r)
		}

		log.Debug(fmt.Sprintf("Secure call is all ok from: %s", r.URL))
		h.ServeHTTP(w, r)
	}
}

func HandleAccount(w http.ResponseWriter, r *http.Request) {
	RenderTemplate(w, r, "account/show", nil)
}

// HandleHome will render the Homepage of this application.
func HandleHome(w http.ResponseWriter, r *http.Request) {

	// Because DefaultServeMux will serve any url that starts with / and not handled
	// via another handler, eg /foo will be handled here.
	if r.URL.Path != "/" {
		http.NotFound(w, r)
		return
	}

	//http.Redirect(w, r, "/account", http.StatusFound)
	RenderTemplate(w, r, "index/home", nil)
}

// Google is currently our only OAuth provider
func HandleOauth(w http.ResponseWriter, r *http.Request) {
	transport.GoogleOauth2(w, r)
}

func HandleOauthCallback(w http.ResponseWriter, r *http.Request) {
	transport.GoogleOauth2Callback(w, r)
}

func HandleView(w http.ResponseWriter, r *http.Request) {
	log.Debug("HandleView")
	u, err := auth.UserFromRequest(r)
	if err != nil {
		// We should not be here without a user so sign out and redirect
		auth.SignOut(w, r)
		http.Redirect(w, r, "/", http.StatusFound)
		log.WithError(err).Info("Unable to find auth user in view.")
		return
	}

	// If not expired, check for an explicit query param to force
	var reset bool
	if !reset {
		queryParams := r.URL.Query()
		force := queryParams.Get("force")
		if "" != force {
			reset, err = strconv.ParseBool(force)
			if err != nil {
				log.WithError(err).
					WithFields(
					log.Fields{"forceQueryParam": force}).
					Warning("Unable to parse force query param")
			}
		}
	}

	// Get a new param struct to set yield params.
	params := u.NewYieldParams()
	params.Reset = reset

	// Yield all the user contacts
	ucs, err := u.Yield(params)
	if err != nil {
		log.WithError(err).
			WithFields(log.Fields{
			"user": u,
		}).Error("Error on user yield contacts")
	}

	var (
		managed []*contact.Contact
		mCount  int64
		umCount int64
	)
	gcs := ucs.ByProviderKey(provider.ProviderGoogle)
	if 0 < len(gcs) {
		for _, c := range gcs {
			if c.IsManaged() {
				managed = append(managed, c)
				mCount += 1
			} else {
				umCount += 1
			}
		}
	}

	m, _ := json.Marshal(managed)
	data := map[string]interface{}{
		"Managed":        m,
		"ManagedCount":   mCount,
		"UnmanagedCount": umCount,
	}
	RenderTemplate(w, r, "view/show", data)
}

func HandleSignOut(w http.ResponseWriter, r *http.Request) {
	auth.SignOut(w, r)
}
