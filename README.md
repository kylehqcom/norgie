# Norgie - a new org nation
___

Typically org charts are a hierarchical top down look at a company. But it's after
2016 now. Company structures should be more about the whole.

Norgie attempts to address this by allowing users to filter their colleagues based
on, location, gender, race and of course direct reporters. In fact with Norgie, you
should be able filter on any `custom` contact value.

## A Warning / Caveat
I created this codebase to continue to increase my knowledge, appreciation and enjoyment
of [Golang](https://golang.org/), along with [MongoDB](https://www.mongodb.com/) and
[Neo4j](https://neo4j.com/). As such, this means that this repository is ***FAR*** from
complete so only use the parts that you want (if any). This message will self destruct
when I'm good n ready =]

___
## Setup

This app is using [Glide](https://glide.sh/) so after checkout, run `glide up` to
install all the dependencies.

Configuration is handled via [Viper](https://github.com/spf13/viper). It attempts to
read it's values from `/etc/config.yml` This file is not part of this repo, nor will
it be due to private info. But here is an example to help

```
# Should viper watch configs and reload on change?
watch: false

# Configs in alphabetical order
contacts:
  sync:
    expiration: 600s # Number of seconds that we deem contacts may be expired

database:
  mongodb:
    dsn:
      user:
      password:
      host: localhost
      port: 27017
    timeout: 3s # Timeout connection in seconds

  neo4j:
    dsn:
      user:
      password:
      host: localhost
      port: 7687
    driver_pool_limit: 10

log:
  level: debug # Refer to https://github.com/Sirupsen/logrus#level-logging

oauth:
  google:
    client_id: "123456789012-someamazingapp.apps.googleusercontent.com"
    client_secret: "so_secret_ssshhhh"
    redirect_url: "/auth/oauth2/callback/google"

session:
  cookie_name: norgie
  cookie_secret: another_really_hexy_secret_string
  session_name: norgie

site:
  http_root: "http://localhost:3000"
```

The app will bomb without a valid MongoDb & Neo4j instance so best to get one running.
Docker anyone?
