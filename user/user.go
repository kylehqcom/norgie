package user

import (
	"encoding/gob"

	log "github.com/Sirupsen/logrus"
	"gitlab.com/kylehqcom/norgie/provider"

	"fmt"

	"sync"

	"gitlab.com/kylehqcom/norgie/contact"
	"golang.org/x/oauth2"
	"gopkg.in/mgo.v2/bson"
)

func init() {
	// Register the user gob for Gorilla Sessions
	gob.Register(&User{})
}

type User struct {
	ID        bson.ObjectId                 `bson:"_id"`
	Name      string                        `bson:"name"`
	Avatar    string                        `bson:"avatar"`
	Email     string                        `bson:"email"`
	Domain    string                        `bson:"domain"`
	Sub       int64                         `bson:"sub"`
	Token     oauth2.Token                  `bson:"token"`
	Providers map[string]*provider.Provider `bson:"providers"`
}

// UserContactProvider interface ensures we return an array of FetchContacts interface objects.
type UserContactProvider interface {
	GetContactProviders(user *User) []provider.FetchContacts
}

type YieldParams struct {
	Reset bool
}

// EnsureProviders will make sure that appropriate contact providers are assigned to a user instance.
func (u *User) EnsureProviders() *User {
	gs := GlobalUserStore()
	for _, p := range provider.DefaultProviders {
		key := p.ProviderKey()
		if _, ok := u.Providers[key]; !ok {
			log.Debug(fmt.Sprintf("Assigning a provider against a user of: %s", key))
			u.Providers[key] = p
			u, err := gs.Save(*u)
			if err != nil {
				log.WithError(err).
					WithFields(log.Fields{
					"user": u,
				}).Error("Unable to save provider against user")
			}
		} else {
			log.Debug(fmt.Sprintf("Provider already assign against user of: %s", key))
		}
	}

	return u
}

// Save on the User type is a convenience method to persist oneself
func (u *User) Save() (*User, error) {
	return GlobalUserStore().Save(*u)
}

// NewUser will return a new User instance
func NewUser() *User {
	u := &User{}
	u.Providers = make(map[string]*provider.Provider)

	return u
}

// NewYieldParams will return a new pointer instance to a yield param struct.
func (u *User) NewYieldParams() *YieldParams {
	return &YieldParams{}
}

// split for interface prep
func (u *User) GetContactProviders() []provider.FetchContacts {
	var fcs []provider.FetchContacts
	for _, p := range u.Providers {
		fcs = append(fcs, p)
	}
	return fcs
}

// Yield contacts from a users contact providers. Currently only one, Google.
func (u *User) Yield(params *YieldParams) (*contact.UserContacts, error) {
	log.Debug("Call: user.Yield()")
	ucs := contact.NewUserContacts()
	ucs.UserID = u.ID.Hex()

	// Ensure that we have at least the default providers before fetching
	u.EnsureProviders()

	// Assign the Fetch params
	fetchParams := provider.NewFetchParams()
	fetchParams.Reset = params.Reset
	fetchParams.Token = u.Token

	// Create a new channel to fetch results for each provider.
	fetchChan := make(chan *provider.FetchResult)
	wg := sync.WaitGroup{}

	for _, p := range u.GetContactProviders() {
		// Check if reset is false and confirm if expiration has passed to update for this provider
		if !fetchParams.Reset {
			if !p.ContactsExpired() {
				log.Debug(fmt.Sprintf("Contacts have expired for provider: %s", p.ProviderKey()))

				// Skip this iteration
				continue
			}
		}

		// Fetch in a anonymous go routine so non blocking & we can defer wait group done.
		wg.Add(1)
		go func(pp provider.FetchContacts, u *User) {
			defer wg.Done()
			go pp.Fetch(fetchChan, fetchParams)
			res := <-fetchChan
			if res.Error != nil {
				log.WithError(res.Error).
					WithFields(log.Fields{
					"user":     u,
					"provider": pp,
				}).Error("Unable to fetch provider contacts")
			} else {
				ucs.Providers[p.ProviderKey()] = res.Result
				if !res.Result.Updated.IsZero() {
					u.Providers[p.ProviderKey()].Updated = res.Result.Updated
				}
			}
		}(p, u)
	}

	// Wait for the go routines to complete
	wg.Wait()

	// Persist the user contacts
	_, err := contact.GlobalContactStore().Upsert(ucs)
	if err != nil {
		// Also save the user object as Yield will update associated provider values
		u.Save()
	}

	return ucs, err
}
