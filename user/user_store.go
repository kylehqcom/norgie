package user

import (
	"gitlab.com/kylehqcom/norgie/config"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"time"

	"fmt"

	log "github.com/Sirupsen/logrus"
	"gitlab.com/kylehqcom/norgie/errors"
)

const (
	DatabaseName   = "norgie"
	UserCollection = "user"
)

// When a user is not found, check Nil error
var NotFound = mgo.ErrNotFound

// The UserStore denotes methods all User Stores should implement. Note the
// FindBySub call - Sub is a 3rd party unique ID. A "sub" in Google's example.
type UserStore interface {
	Find(string) (*User, error)
	FindByEmail(string) (*User, error)
	FindBySub(int64) (*User, error)
	Save(User) error
}

var globalUserStore mongoDBUserStore

// A mongoDBUserStore is used to interact between a MongoDB and a Norgie User
type mongoDBUserStore struct {
	Session *mgo.Session
}

// db will get a database instance from an existing MongoDB session
func (m mongoDBUserStore) db() *mgo.Database {
	return m.Session.Copy().DB(DatabaseName)
}

// ensureIndexes will create the required mongoDB indexes
func (m mongoDBUserStore) ensureIndexes() {
	log.Debug("Call: mongoDBUserStore.ensureIndexes()")
	userEmailIdx := mgo.Index{
		Key:        []string{"email"},
		Unique:     true,
		DropDups:   true,
		Background: true,
		Sparse:     true,
	}
	err := m.db().C(UserCollection).EnsureIndex(userEmailIdx)
	if err != nil {
		log.WithFields(log.Fields{
			"index":     "userEmailIdx",
			"index_key": "email",
		}).WithError(err).Error("Failed to create mongodb index")
	}

	userSubIdx := mgo.Index{
		Key:        []string{"sub"},
		Unique:     true,
		DropDups:   true,
		Background: true,
		Sparse:     true,
	}
	err = m.db().C(UserCollection).EnsureIndex(userSubIdx)
	if err != nil {
		log.WithFields(log.Fields{
			"index":     "userSubIdx",
			"index_key": "sub",
		}).WithError(err).Error("Failed to create mongodb index")
	}

	log.Debug("mongoDBUserStore.ensureIndexes complete. %s")
}

func BootstrapDB() {
	log.Debug("Bootstrapping mongoDB user store")

	// Get mongoDB config details
	dbConfig := config.Instance().Sub("database.mongodb")
	timeoutStr := dbConfig.GetString("timeout")
	if timeoutStr == "" {
		timeoutStr = "5s"
	}
	timeout, _ := time.ParseDuration(timeoutStr)

	// And the DSN values
	dsnConfig := dbConfig.Sub("dsn")
	u := dsnConfig.GetString("user")
	p := dsnConfig.GetString("password")
	host := dsnConfig.GetString("host")
	port := dsnConfig.GetString("port")

	// Check for a user/password combination
	up := ""
	if u != "" {
		up = fmt.Sprintf("%s:%s@", u, p)
	}

	dsn := fmt.Sprintf("mongodb://%s%s:%s", up, host, port)
	log.Debug(fmt.Sprintf("Opening mongoDB on mongodb://%s:%s", host, port))

	// Get a mongoDB instance
	session, err := mgo.DialWithTimeout(dsn, timeout)
	if err != nil {

		// reset the Global session store
		globalUserStore.Session = nil

		// Fatal this - we need a dbase
		log.WithFields(log.Fields{
			"dsn": dsnConfig,
		}).WithError(err).Fatal("Unable to create mongoDB session")
	}

	globalUserStore = mongoDBUserStore{
		Session: session,
	}

	// Ensure we have the correct indexes on our Collections
	globalUserStore.ensureIndexes()

	log.Debug("Bootstrap mongoDB user store complete")
}

// GlobalUserStore returns Norgies global store for user persistence.
func GlobalUserStore() mongoDBUserStore {
	// Bootstrap the mongoDB if none found
	if globalUserStore.Session == nil {
		BootstrapDB()
	}

	return globalUserStore
}

// Find will return a user from a mongoDB generated UUID
func (m mongoDBUserStore) Find(HexID string) (*User, error) {
	u := NewUser()
	if bson.IsObjectIdHex(HexID) {
		err := m.db().C(UserCollection).FindId(bson.ObjectIdHex(HexID)).One(u)
		return u, err
	} else {
		return u, errors.ErrInvalidHexID
	}
}

// FindByEmail will attempt to return a User by a "google" email address.
func (m mongoDBUserStore) FindByEmail(email string) (*User, error) {
	u := NewUser()
	err := m.db().C(UserCollection).Find(bson.M{"email": email}).One(u)
	return u, err
}

// FindBySub will attempt to return a User by a 3rd party ID, their "sub"
func (m mongoDBUserStore) FindBySub(sub int64) (*User, error) {
	u := NewUser()
	err := m.db().C(UserCollection).Find(bson.M{"sub": sub}).One(u)
	return u, err
}

// Save will persist a User to the MongoDB via an Upsert
func (m mongoDBUserStore) Save(u User) (*User, error) {
	if !u.ID.Valid() {
		u.ID = bson.NewObjectId()
	}

	_, err := m.db().C(UserCollection).UpsertId(u.ID, bson.M{"$set": u})
	if mgo.IsDup(err) {
		log.WithFields(log.Fields{
			"user": u,
		}).WithError(err).Error("Duplicate user key found")

		// And reset error as we have handled
		err = nil
	}

	return &u, err
}
