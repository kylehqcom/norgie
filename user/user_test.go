package user

import (
	"net/http"
	"testing"

	"net/http/httptest"

	"fmt"

	"github.com/gorilla/sessions"
	"gitlab.com/kylehqcom/norgie/provider"
	"gopkg.in/mgo.v2/bson"
)

func createStore() *sessions.FilesystemStore {
	s := sessions.NewFilesystemStore("", []byte("keypair"))
	s.Options.Path = "/"
	return s
}

func createRequest() *http.Request {
	r, err := http.NewRequest("GET", "http://www.example.com", nil)
	if err != nil {
		panic(err)
	}

	return r
}

func createWriter() *httptest.ResponseRecorder {
	return httptest.NewRecorder()
}

func TestAuthenticatedUser(t *testing.T) {

	r := createRequest()
	w := createWriter()
	sess, err := createStore().New(r, "foo")

	id := bson.NewObjectId()
	sub := int64(10)
	email := "foo@bar.com"
	p := provider.NewGoogle()
	u := NewUser()
	u.ID = id
	u.Sub = sub
	u.Email = email
	u.Providers[p.Key()] = *p

	sess.Values["user"] = u
	err = sess.Save(r, w)
	if err != nil {
		t.Fatal(err)
	}

	fromSession, ok := sess.Values["user"].(*User)
	if !ok {
		t.Error("User not cast correctly")
		t.Fail()
	}

	if fromSession.ID != id {
		t.Error("ID values should match")
	}

	if fromSession.Sub != sub {
		t.Error("Sub values should match")
	}

	if fromSession.Email != email {
		t.Error("email values should match")
	}

	val, ok := fromSession.Providers[provider.ProviderGoogle]
	if !ok {
		t.Error("unable to get map from google key")
	}

	if (fmt.Sprintf("%T", val)) != fmt.Sprintf("%T", *p) {
		t.Error("providers types do not match")
	}

	if val.Name != p.Name {
		t.Error("provider name mismatch")
	}
}
