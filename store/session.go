// The store package deals with a global session store
// for your project, using gorilla/sessions as its
// backbone and Viper for default config values
package store

import (
	"net/http"

	"github.com/gorilla/sessions"

	"crypto/sha512"
	"encoding/base32"
	"encoding/hex"
	"strings"

	"github.com/gorilla/securecookie"
	"gitlab.com/kylehqcom/norgie/config"
)

var (
	cs   = sessions.NewCookieStore([]byte(config.Instance().GetString("session.cookie_secret")))
	name string
)

func init() {
	cs.Options = &sessions.Options{
		Path:     "/",
		MaxAge:   3600, // 1 hour
		HttpOnly: true,
	}
}

// Session will return the global session object
func Session(r *http.Request) (*sessions.Session, error) {
	return cs.Get(r, SessionName())
}

// SessionName will return the global session name
func SessionName() string {
	if name == "" {
		name = config.Instance().GetString("session.session_name")
	}

	return name
}

// GenerateID will return a random unique identifier string
func GenerateID() string {
	return strings.TrimRight(
		base32.StdEncoding.EncodeToString(
			securecookie.GenerateRandomKey(32)), "=")
}

// GenerateXsrf will return an Xsrf string - useful for Oauth flows
func GenerateXsrf(ID, uri string) string {
	sum := sha512.Sum512([]byte(config.Instance().GetString("session.cookie_secret") + ID + uri))
	return hex.EncodeToString(sum[:])
}
