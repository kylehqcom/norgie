package main

import (
	"bufio"

	"net/http"

	"os"

	log "github.com/Sirupsen/logrus"
	"github.com/gorilla/context"
	"gitlab.com/kylehqcom/norgie/config"
	"gitlab.com/kylehqcom/norgie/contact"
	"gitlab.com/kylehqcom/norgie/templates"
	"gitlab.com/kylehqcom/norgie/user"
)

func init() {
	// Pretty up the cli =]
	banner()

	// Load configuration.
	config.Load()

	// Set logging values for the application.
	logging()

	// Bootstrap the dbases
	user.BootstrapDB()
	contact.BootstrapDB()
}

func logging() {
	logLevel := config.Instance().GetString("log.level")
	switch logLevel {
	case "debug":
		log.SetLevel(log.DebugLevel)
	default:
		log.SetLevel(log.InfoLevel)
	}
}

// Because ascii is the new png
func banner() {
	inFile, _ := os.Open("./resources/banner.txt")
	defer func() {
		_ = inFile.Close()
	}()

	scanner := bufio.NewScanner(inFile)
	scanner.Split(bufio.ScanLines)
	for scanner.Scan() {
		println(scanner.Text())
	}
}

func main() {
	http.HandleFunc("/auth/oauth2/google", templates.HandleOauth)
	http.HandleFunc("/auth/oauth2/callback/google", templates.HandleOauthCallback)

	http.HandleFunc("/view", templates.Secure(templates.HandleView))
	http.HandleFunc("/account", templates.Secure(templates.HandleAccount))

	http.Handle("/assets/", http.FileServer(http.Dir("./")))
	http.Handle("/favicon.ico", http.FileServer(http.Dir("./")))

	http.HandleFunc("/", templates.HandleHome)
	http.HandleFunc("/signout", templates.HandleSignOut)

	// Context ClearHandler due to gorilla sessions GC
	log.Fatal(http.ListenAndServe(":3000", context.ClearHandler(http.DefaultServeMux)))
}
