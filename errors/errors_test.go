package errors

import "testing"

func TestIsSessionError(t *testing.T) {
	if !IsSessionError(ErrSessionParsing) {
		t.Error("Session Error was not compared correctly")
	}

	if IsUserError(ErrSessionParsing) {
		t.Error("Session Error was compared as a User error")
	}

	if IsProviderError(ErrSessionParsing) {
		t.Error("Session Error was compared as a Provider error")
	}
}

func TestIsUserError(t *testing.T) {
	if IsSessionError(ErrUserNotFound) {
		t.Error("User Error was compared as a Session error")
	}

	if !IsUserError(ErrUserNotFound) {
		t.Error("User Error was not compared correctly")
	}

	if IsProviderError(ErrUserNotFound) {
		t.Error("User Error was compared as a Provider error")
	}
}

func TestIsProviderError(t *testing.T) {
	if IsSessionError(ErrProviderNotFound) {
		t.Error("Provider Error was compared as a Session error")
	}

	if IsUserError(ErrProviderNotFound) {
		t.Error("Provider Error was compared as a User error")
	}

	if !IsProviderError(ErrProviderNotFound) {
		t.Error("Provider Error was not compared correctly")
	}
}
