package errors

type SessionError struct {
	s string
}

var (
	// Session Errors
	ErrSessionParsing = NewSessionError("Unable to parse user from session.")
)

func (e SessionError) Error() string {
	return e.s
}

func IsSessionError(err error) bool {
	_, ok := err.(*SessionError)

	return ok
}

func NewSessionError(errorMessage string) error {
	return &SessionError{errorMessage}
}
