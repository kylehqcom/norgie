package errors

type ProviderError struct {
	s string
}

var (
	// Provider Errors
	ErrProviderNotFound = NewProviderError("No provider found.")
	ErrTooManyRequests  = NewProviderError("To many request to return provider contacts.")
)

func (e ProviderError) Error() string {
	return e.s
}

func IsProviderError(err error) bool {
	_, ok := err.(*ProviderError)
	return ok
}

func NewProviderError(errorMessage string) error {
	return &ProviderError{errorMessage}
}
