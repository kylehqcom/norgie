package errors

type UserError struct {
	s string
}

var (
	// User Errors
	ErrInvalidHexID            = NewUserError("ID given to Find was not a valid hex representation.")
	ErrNoUserId                = NewUserError("No user id present.")
	ErrUserNotFound            = NewUserError("No user found.")
	ErrUserTokenInvalid        = NewUserError("No user token is invalid.")
	ErrUserMissingRefreshToken = NewUserError("The user has no refresh token assigned.")
)

func (e UserError) Error() string {
	return e.s
}

func IsUserError(err error) bool {
	_, ok := err.(*UserError)
	return ok
}

func NewUserError(errorMessage string) error {
	return &UserError{errorMessage}
}
