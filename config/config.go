package config

import (
	"fmt"

	"github.com/spf13/viper"
)

const (
	CLIENT_ID     = "client_id"
	CLIENT_SECRET = "client_secret"
	REDIRECT_URL  = "redirect_url"

	OAUTH_GOOGLE = "oauth.google"

	SITE_HTTP_ROOT = "site.http_root"
)

// Config is a viper instance to get/set configs
var c *viper.Viper

func Instance() *viper.Viper {
	if nil == c {
		c = viper.New()
	}
	return c
}

func Load() {
	c = Instance()
	c.SetConfigName("config")
	c.AddConfigPath("./etc/")

	// Find and read the config file
	err := c.ReadInConfig()
	if err != nil {
		// Handle errors reading the config file
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}

	// Should we "watch" for config changes
	if c.GetBool("watch") {
		c.WatchConfig()
	}
}
